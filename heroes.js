const req = new XMLHttpRequest();
req.onload = function () {
    const data = JSON.parse(req.responseText);
    populatePage(data);
};
req.open('GET', 'https://raw.githubusercontent.com/ewomackQA/JSONDataRepo/master/example.json');
req.send(null);

/**
 * Populates the page from supplied squad data.
 * @param {*} data Squad data
 */
function populatePage({squadName, members, ...everythingElse }) {
    // Container refs
    const squadDetailsEl = document.getElementById('squadDetails');
    const membersEl = document.getElementById('members');

    // Populate header
    document.getElementById('squadName').innerText = squadName;

    // Generate top-level squad details
    for (let k in everythingElse) {
        squadDetailsEl.append(getKeyValueParagraph(k, everythingElse[k]));
    }

    // Generate members
    for (let member of members) {
        membersEl.append(getSquadMemberElement(member));
    }
}

function getSquadMemberElement(member) {
    const wrapper = document.createElement('div');
    wrapper.className = 'card m-3';

    const title = document.createElement('h3');
    title.className = 'p-2 border-bottom';
    title.innerText = member.name;
    wrapper.append(title);

    for(let key in member) {
        wrapper.append(getKeyValueParagraph(key, member[key]));
    }

    return wrapper;
}

function getKeyValueParagraph(key, value) {
    const el = document.createElement('p');
    el.innerHTML = '<b>' + key + '</b>: ' + value;
    return el;
}
